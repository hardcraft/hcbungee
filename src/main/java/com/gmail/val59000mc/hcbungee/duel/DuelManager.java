package com.gmail.val59000mc.hcbungee.duel;

import com.gmail.val59000mc.hcbungee.HCBungee;
import com.gmail.val59000mc.hcbungee.sockets.Sockets;
import com.gmail.val59000mc.hcsocket.bungee.event.BungeeSocketEvent;
import com.gmail.val59000mc.hcsocket.bungee.event.DuelRequestEvent;
import com.gmail.val59000mc.hcsocket.common.dto.DuelRequest;
import com.gmail.val59000mc.hcsocket.common.dto.EventType;
import com.gmail.val59000mc.hcsocket.common.dto.NetworkPlayer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class DuelManager {

    private HCBungee plugin;
    private Sockets sockets;

    public DuelManager(HCBungee plugin, Sockets sockets) {
        this.plugin = plugin;
        this.sockets = sockets;
        this.plugin.getProxy().getPluginManager().registerCommand(plugin, new DuelCommand(this));

    }

    public void sendDuelRequest(ProxiedPlayer requester, ProxiedPlayer requested) {
        DuelRequest duelRequest = new DuelRequest(toNetworkPlayer(requester), toNetworkPlayer(requested));
        BungeeSocketEvent event = new DuelRequestEvent("duels", EventType.INIT_DUEL_REQUEST, duelRequest);
        sockets.sendToServerOnChannel(requested.getServer().getInfo(), event);
    }

    private NetworkPlayer toNetworkPlayer(ProxiedPlayer player){
        return new NetworkPlayer(player.getUniqueId(), player.getName());
    }

}
