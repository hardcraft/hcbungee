package com.gmail.val59000mc.hcbungee.duel;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class DuelCommand extends Command{

	private DuelManager duelManager;

	public DuelCommand(DuelManager duelManager) {
		super("duel");
		this.duelManager = duelManager;
	}

	@Override
	public void execute(CommandSender sender, String[] args) {

		if(sender instanceof ProxiedPlayer && args.length > 0){
			String playerName = args[0];
			ProxiedPlayer target = ProxyServer.getInstance().getPlayer(playerName);
			if(target != null){
				duelManager.sendDuelRequest((ProxiedPlayer) sender, target);
			}
		}

	}

}
