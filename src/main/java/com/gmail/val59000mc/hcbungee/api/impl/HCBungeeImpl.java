package com.gmail.val59000mc.hcbungee.api.impl;

import com.gmail.val59000mc.hcbungee.api.HCBungeeAPI;
import com.gmail.val59000mc.hcbungee.api.HCMySQLAPI;
import com.gmail.val59000mc.hcbungee.common.Constants;
import com.gmail.val59000mc.hcbungee.database.DummySqlImpl;
import com.gmail.val59000mc.hcbungee.database.MySQLDatabase;
import com.gmail.val59000mc.hcbungee.util.Log;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;

public class HCBungeeImpl implements HCBungeeAPI {

    private Plugin plugin;
    private Configuration config;

    // sub APIs
    private HCMySQLAPI mysql;

    public HCBungeeImpl(Plugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public HCBungeeAPI loadNetwork() {
        createPluginDir();
        loadConfig();
        loadLogger();
        loadDatabase();
        return this;
    }

    @Override
    public Configuration getConfig() {
        return config;
    }

    private void createPluginDir() {
        if (!plugin.getDataFolder().exists())
            plugin.getDataFolder().mkdir();
    }

    private void loadConfig() {

        ConfigurationProvider yaml = ConfigurationProvider.getProvider(YamlConfiguration.class);
        String fileName = "config.yml";

        File file = new File(
            plugin.getDataFolder(),
            fileName
        );

        if (!file.exists()) {
            try (InputStream in = plugin.getResourceAsStream(fileName)) {
                java.nio.file.Files.copy(in, file.toPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            config = yaml.load(file);
        } catch (IOException e) {
            throw new IllegalStateException("Could not load config !", e);
        }

    }

    private void loadLogger() {
        Log.setLogger(
                plugin.getLogger(),
                config.getBoolean("logger.debug", Constants.DEFAULT_DEBUG_ENABLED),
                config.getBoolean("logger.debugSQL", Constants.DEFAULT_DEBUG_SQL_ENABLED),
                config.getBoolean("logger.debugTasks", Constants.DEFAULT_DEBUG_TASKS_ENABLED)
        );

        Log.info("Configuration successfully loaded !");
    }

    private void loadDatabase() {
        // Load MySQL
        if (config.getBoolean("mysql.enabled", Constants.DEFAULT_MYSQL_ENABLED)) {
            mysql = new MySQLDatabase(this, config);
            try {
                ((MySQLDatabase) mysql).initialize();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            if(mysql.isEnabled()){
                Log.info("Sucessfuly connected to MySQL database");
            }else{
                Log.info("Could not connect to MySQL database. Defaulting to dummy database implementation.");
                mysql = new DummySqlImpl();
            }

        } else {
            this.mysql = new DummySqlImpl();
        }
    }

    @Override
    public HCMySQLAPI getMySQLAPI() {
        return mysql;
    }

    @Override
    public Plugin getPlugin() {
        return plugin;
    }
}
