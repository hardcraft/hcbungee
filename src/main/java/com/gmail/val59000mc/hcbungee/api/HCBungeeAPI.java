package com.gmail.val59000mc.hcbungee.api;

import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;

public interface HCBungeeAPI {

    HCBungeeAPI loadNetwork();

    Configuration getConfig();

    HCMySQLAPI getMySQLAPI();

    Plugin getPlugin();
}
