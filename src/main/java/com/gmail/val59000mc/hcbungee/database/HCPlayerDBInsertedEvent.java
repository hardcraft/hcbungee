package com.gmail.val59000mc.hcbungee.database;

import com.gmail.val59000mc.hcbungee.api.HCBungeeAPI;
import com.gmail.val59000mc.hcbungee.event.HCBungeeEvent;
import com.gmail.val59000mc.hcbungee.players.BungeePlayer;

public class HCPlayerDBInsertedEvent extends HCBungeeEvent{

	private BungeePlayer bngPlayer;
	
	public HCPlayerDBInsertedEvent(HCBungeeAPI api, BungeePlayer bngPlayer) {
		super(api);
		this.bngPlayer = bngPlayer;
	}

	public BungeePlayer getBngPlayer() {
		return bngPlayer;
	}

	
}
