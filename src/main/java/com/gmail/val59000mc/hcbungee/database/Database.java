package com.gmail.val59000mc.hcbungee.database;

import com.gmail.val59000mc.hcbungee.util.Log;
import com.sun.rowset.CachedRowSetImpl;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import net.md_5.bungee.api.plugin.Plugin;

import javax.sql.DataSource;
import javax.sql.rowset.CachedRowSet;
import java.sql.*;

public abstract class Database {

    private HikariDataSource dataSource;
    private Plugin clientPlugin;

    public DataSource getDatasource(){
        return dataSource;
    }

    /**
     * Initialize a Database connection.
     *
     * @param className Database type to connect to.
     * @param jdbcURL JDBC url to connect to.
     * @param username Username to log in with.
     * @param password Password to authenticate user.
     */
    protected Database(String className, String jdbcURL, String username, String password, Plugin clientPlugin) {
    	
    	this.clientPlugin = clientPlugin;
    	
        try {
            Class.forName(className);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return;
        }

        HikariConfig config = new HikariConfig();

        config.setDriverClassName(className);
        
        config.setJdbcUrl(jdbcURL);

        config.setUsername(username);
        config.setPassword(password);

        config.setLeakDetectionThreshold(10000);
        config.setMaxLifetime(120000);
        config.setIdleTimeout(30000);
        config.setMaximumPoolSize(2);
        config.setConnectionTimeout(10000);
        config.setInitializationFailFast(false);

        try {
            dataSource = new HikariDataSource(config);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Connection c = getConnection();
        if (c == null) {
        	Log.err("[HCGamesLib] Unable to connect to the database");
        } else
        {
            try {
                c.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Close all current connections and disconnect from the database.
     */
    public void disconnect() {
        dataSource.close();
    }

    /**
     * Query the database.
     *
     * @param preparedStatement Query to submit.
     * @return A CachedRowSet with data from query, or null when an error has occurred.
     * @throws SQLException 
     */
    public CachedRowSet query(final PreparedStatement preparedStatement) throws SQLException {

        try {
            ResultSet resultSet = preparedStatement.executeQuery();

            CachedRowSet cachedRowSet = new CachedRowSetImpl();
            cachedRowSet.populate(resultSet);
            resultSet.close();
            return cachedRowSet;
        }finally{
        	preparedStatement.close();
        	preparedStatement.getConnection().close();
        }
    }
    

    /**
     * Execute a statement async in the database.
     *
     * @param preparedStatement The statement to execute.
     * @throws SQLException 
     */
    public int execute(final PreparedStatement preparedStatement) throws SQLException {
        try {
            return preparedStatement.executeUpdate();
        }finally {
            preparedStatement.close();
            preparedStatement.getConnection().close();
        }
    }
    
    /**
     * Execute a statement in the database
     * and return the generated ids if any
     * @param preparedStatement The statement to execute.
     * @throws SQLException 
     */
    public CachedRowSet executeWithGeneratedIds(final PreparedStatement preparedStatement) throws SQLException {
        try {
            preparedStatement.execute();
            
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            CachedRowSet cachedRowSet = new CachedRowSetImpl();
            cachedRowSet.populate(resultSet);
            resultSet.close();
            return cachedRowSet;
            
        }finally{
            preparedStatement.close();
            preparedStatement.getConnection().close();
        }
    }

    /**
     * Put a given string query into a PreparedStatement object.
     *
     * @param query Query/statement to put into object.
     * @param vars Variables to switch ? with in order.
     * @return A PreparedStatement object.
     * @throws SQLException 
     */
    public PreparedStatement prepareStatement(String query, Object... vars) throws SQLException {
        
        Connection c = getConnection();
        PreparedStatement preparedStatement = c.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

        int x = 0;

        if (query.contains("?") && vars.length != 0) {
            for (Object var : vars) {
                x++;
                preparedStatement.setObject(x, var);
            }
        }

        return preparedStatement;
    }

    /**
     * Get a connection from the datapool.
     *
     * @return A connection, or null when a connection isn't available.
     */
    public Connection getConnection() {
        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

}