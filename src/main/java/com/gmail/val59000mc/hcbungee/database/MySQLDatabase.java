package com.gmail.val59000mc.hcbungee.database;

import com.gmail.val59000mc.hcbungee.api.HCBungeeAPI;
import com.gmail.val59000mc.hcbungee.api.HCMySQLAPI;
import com.gmail.val59000mc.hcbungee.util.Log;
import net.md_5.bungee.config.Configuration;

import javax.sql.rowset.CachedRowSet;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class MySQLDatabase extends Database implements HCMySQLAPI{

    /*
     * Construct a MySQL database instance.
     *
     * @param plugin   Plugin for the schedulers to be assigned to.
     * @param cfg	Config file for database
     */
	private HCBungeeAPI api;
	
	
	// Stored queries

	private String selectGameByNameSQL;
	private String selectPlayerByUUIDSQL;
	private String selectPlayerByIdSQL;
	private String selectPlayerByNameSQL;
	private String selectMultiplePlayersByUUIDSQL;
	private String selectGlobalConfig;
	private String replaceIntoGlobalConfig;
	
	private String month;
	private String year;
	
	private boolean enabled;



    public MySQLDatabase(HCBungeeAPI api, Configuration cfg) {
    	
        super(
        	  "com.mysql.jdbc.Driver", 
        	  "jdbc:mysql://" + cfg.getString("mysql.hostname") +  ":" + cfg.getString("mysql.port") +  "/" +  cfg.getString("mysql.database") , 
        	  cfg.getString("mysql.username"), 
        	  cfg.getString("mysql.password"), 
        	  api.getPlugin()
        );
        
        this.api = api;
        this.enabled = false;

    }


	public void initialize() throws SQLException{
		
		// Execute "create_player.sql" script
		String player = readQueryFromResource("sql/create_player.sql");
		execute(prepareStatement(player));

		selectPlayerByUUIDSQL = readQueryFromResource("sql/select_player_by_uuid.sql");
		selectPlayerByIdSQL = readQueryFromResource("sql/select_player_by_id.sql");
		selectPlayerByNameSQL = readQueryFromResource("sql/select_player_by_name.sql");
		selectMultiplePlayersByUUIDSQL = readQueryFromResource("sql/select_multiple_players_by_uuid.sql");

		enabled = true;
	}
	
	@Override
	public PreparedStatement prepareStatement(String query, Object... vars) throws SQLException {
		
		if(vars == null){
			Log.debugSQL("Preparing mysql query '"+query+"'");
		}else{
			Log.debugSQL("Preparing mysql query '"+query+"' with values "+Arrays.toString(vars)+"");
		}
		
        return super.prepareStatement(query, vars);
    }

	@Override
	public String readQueryFromResource(String resourceName){
		InputStream resource = api.getPlugin().getResourceAsStream(resourceName);
		@SuppressWarnings("resource")
		Scanner s = new Scanner(resource).useDelimiter("\\A");
	    String query = s.hasNext() ? s.next() : "";
	  	s.close();
	  	return query;
	}
	
	// API

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public void handleException(SQLException e) {
		Log.err("An error occurred while performing an SQL query on the database. Stack strace below.");
		e.printStackTrace();
	}

	@Override
	public CachedRowSet selectPlayerByUUID(String uuid) throws SQLException {
		return this.query(this.prepareStatement(selectPlayerByUUIDSQL, uuid));
	}

	@Override
	public CachedRowSet selectPlayerById(int id) throws SQLException {
		return this.query(this.prepareStatement(selectPlayerByIdSQL, id));
	}

	@Override
	public CachedRowSet selectPlayerByName(String name) throws SQLException {
		return this.query(this.prepareStatement(selectPlayerByNameSQL, name));
	}

	@Override
	public CachedRowSet selectMultiplePlayersByUUID(List<String> uuid) throws SQLException {
		
		String multipleQuery = selectMultiplePlayersByUUIDSQL;
		
		// Adding as many "?" in the SQL "IN" clause as there is uuid values
		StringBuilder qMarks = new StringBuilder();
		int size = uuid.size();
		if(size > 0){
			for(int i=0 ; i<size ; i++){
				qMarks.append("?,");
			}
			
			multipleQuery = multipleQuery.replace("?", qMarks.deleteCharAt(qMarks.length()-1).toString());
		}
		
		return this.query(this.prepareStatement(multipleQuery, uuid.toArray(new String[uuid.size()])));
	}


}
