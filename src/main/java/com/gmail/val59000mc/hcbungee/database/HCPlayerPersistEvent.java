package com.gmail.val59000mc.hcbungee.database;

import com.gmail.val59000mc.hcbungee.api.HCBungeeAPI;
import com.gmail.val59000mc.hcbungee.event.HCBungeeEvent;
import com.gmail.val59000mc.hcbungee.players.BungeePlayer;

public class HCPlayerPersistEvent extends HCBungeeEvent{

	/**
	 * This is a temporary bngPlayer not tracked by the plaeyr manager
	 * It stores the difference of incremental statistics between now and the last time the actual player was persisted
	 * To get the actual player, one must use getPmApi().getBngPlayer(uuid)
	 */
	private BungeePlayer bngPlayer;

	public HCPlayerPersistEvent(HCBungeeAPI api, BungeePlayer bngPlayer) {
		super(api);
		this.bngPlayer = bngPlayer;
	}

	public BungeePlayer getBngPlayer() {
		return bngPlayer;
	}

	
}
