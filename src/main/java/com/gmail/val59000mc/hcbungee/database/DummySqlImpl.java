package com.gmail.val59000mc.hcbungee.database;


import com.gmail.val59000mc.hcbungee.api.HCMySQLAPI;

import javax.sql.rowset.CachedRowSet;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class DummySqlImpl implements HCMySQLAPI {

	@Override
	public boolean isEnabled() {
		return false;
	}

	@Override
	public String readQueryFromResource(String resourceName) {
		throw new IllegalArgumentException("mysql is disabled");
	}

	@Override
	public CachedRowSet query(PreparedStatement preparedStatement) {
		throw new IllegalArgumentException("mysql is disabled");
	}

	@Override
	public CachedRowSet executeWithGeneratedIds(final PreparedStatement preparedStatement){
		throw new IllegalArgumentException("mysql is disabled");
	}

	@Override
	public int execute(PreparedStatement preparedStatement) {
		throw new IllegalArgumentException("mysql is disabled");
	}

	@Override
	public PreparedStatement prepareStatement(String query, Object... vars) {
		throw new IllegalArgumentException("mysql is disabled");
	}

	@Override
	public void handleException(SQLException e) {
		throw new IllegalArgumentException("mysql is disabled");
	}

	@Override
	public CachedRowSet selectPlayerByUUID(String uuid) {
		throw new IllegalArgumentException("mysql is disabled");
	}

	@Override
	public CachedRowSet selectPlayerById(int id) throws SQLException {
		throw new IllegalArgumentException("mysql is disabled");
	}

	@Override
	public CachedRowSet selectPlayerByName(String name) {
		throw new IllegalArgumentException("mysql is disabled");
	}

	@Override
	public CachedRowSet selectMultiplePlayersByUUID(List<String> uuid) {
		throw new IllegalArgumentException("mysql is disabled");
	}
}
