package com.gmail.val59000mc.hcbungee.announcer;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import com.gmail.val59000mc.hcbungee.HCBungee;
import com.gmail.val59000mc.hcbungee.util.Texts;
import com.google.common.base.Preconditions;

import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.scheduler.ScheduledTask;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

public class Announcer {

	private HCBungee plugin;
	private Logger logger;
	
	private String prefix;
	
	/**
	 * To store all the messages when loading/reloading the plugin
	 */
	private List<String> messages;
	
	/**
	 * To store the messages not yet displayed in the random order
	 */
	private List<String> messagesNotYetDisplayed;
	
	private int delay;
	private ScheduledTask task;
	private Random r;
	
	public Announcer(){
		
	}

	public Announcer(HCBungee plugin) {
		this.plugin = plugin;
		this.logger = plugin.getLogger();
		this.r = new Random();
	}

	public void enable() {
		this.reload();
		plugin.getProxy().getPluginManager().registerCommand(plugin, new AnnouncerCommand(this));
	}

	public boolean reload() {

		File file = new File(plugin.getDataFolder(), "announcer.yml");
	     
        if (!file.exists()) {
            try (InputStream in = plugin.getResourceAsStream("announcer.yml")) {
                java.nio.file.Files.copy(in, file.toPath());
            } catch (IOException e) {
    			logger.severe("Could create announcer default config file !");
                e.printStackTrace();
                return false;
            }
        }
        
		Configuration cfg;
		try {
			cfg = ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);
			List<String> newMessages = cfg.getStringList("messages");
			
			if(newMessages == null){
				logger.severe("Cannot load messages list from the config file !");
				return false;
			}

			if(newMessages.size() == 0){
				logger.severe("The message list is empty !");
				return false;
			}
			
			messages = newMessages;
			messagesNotYetDisplayed = new ArrayList<>(newMessages);
			prefix = cfg.getString("prefix", "&b[INFO] ");
			delay = cfg.getInt("delay",1200);
			
			if(task != null){
				task.cancel();
			}
			
			task = plugin.getProxy().getScheduler().schedule(plugin, ()->{
				this.announceNext();
			}, 0, delay, TimeUnit.SECONDS);
			
			return true;
			
		} catch (IOException e) {
			logger.severe("Could load announcer yml config file !");
			e.printStackTrace();
			return false;
		}
		
	}

	private void announceNext() {
		Preconditions.checkArgument(messages.size() > 0, "messages list cannot be empty");
		if(messagesNotYetDisplayed.size() == 0){
			messagesNotYetDisplayed = new ArrayList<String>(messages);
		}
		int randomIndex = r.nextInt(messagesNotYetDisplayed.size());
		String message = messagesNotYetDisplayed.remove(randomIndex);
		for(ProxiedPlayer player : plugin.getProxy().getPlayers()){
			player.sendMessage(Texts.toJson(prefix+message));
		}
		
	}
	
}
