package com.gmail.val59000mc.hcbungee.announcer;

import com.gmail.val59000mc.hcbungee.util.Texts;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

public class AnnouncerCommand extends Command{

	private Announcer announcer;
	
	public AnnouncerCommand(Announcer announcer) {
		super("announcer", "hcbungee.announcer.admin");
		this.announcer = announcer;
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if(args.length == 1 && args[0].equals("reload")){
			boolean reloaded = announcer.reload();
			if(reloaded){
				message(sender,"&aAnnouncer reloaded !");
			}else{
				message(sender,"&cReload failed, see console");
			}
		}else{
			message(sender,"&cSyntaxe /announcer reload");
		}
	}

	private void message(CommandSender sender, String string) {
		sender.sendMessage(Texts.toJson(string));
	}

}
