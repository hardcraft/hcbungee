package com.gmail.val59000mc.hcbungee.sockets;

import com.gmail.val59000mc.hcbungee.HCBungee;
import com.gmail.val59000mc.hcsocket.bungee.HCSocketBungee;
import com.gmail.val59000mc.hcsocket.bungee.event.BungeeSocketEvent;
import com.gmail.val59000mc.hcsocket.common.util.DataUtil;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.plugin.Plugin;

public class Sockets {

    private HCBungee plugin;
    private boolean enabled = false;
    private HCSocketBungee hcSocket;

    public Sockets(HCBungee plugin) {
        this.plugin = plugin;
        Plugin hcSocket = plugin.getProxy().getPluginManager().getPlugin("HCSocket");
        if(hcSocket != null){
            this.enabled = true;
            this.hcSocket = (HCSocketBungee) hcSocket;
        }
    }

    private void checkEnabled(){
        if(!enabled)
            throw new IllegalStateException("HCSocket is not enabled !");
    }

    public void sendToServerOnChannel(ServerInfo serverInfo, BungeeSocketEvent socketEvent) {
        checkEnabled();
        hcSocket.getAPI().sendDataToClient(serverInfo, socketEvent.getChannel(), DataUtil.toData(socketEvent.getType(), socketEvent.getParameters()));
    }

}
