package com.gmail.val59000mc.hcbungee.players;

import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.lang.ref.WeakReference;
import java.util.UUID;

public class BungeePlayer {

    protected int id;
    protected UUID uuid;
    protected String name;
    protected WeakReference<ProxiedPlayer> player;

    public BungeePlayer(ProxiedPlayer proxiedPlayer) {
        this.name = proxiedPlayer.getName();
        this.uuid = proxiedPlayer.getUniqueId();
        this.player = new WeakReference<ProxiedPlayer>(proxiedPlayer);
    }

    public int getId() {
        return id;
    }

    public UUID getUuid() {
        return uuid;
    }

    public String getName() {
        return name;
    }

    public ProxiedPlayer getPlayer(){
        return player.get();
    }

    public Boolean isOnline(){
        ProxiedPlayer p = getPlayer();
        return p != null && p.isConnected();
    }
}
