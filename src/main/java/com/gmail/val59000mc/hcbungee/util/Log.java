package com.gmail.val59000mc.hcbungee.util;

import java.util.logging.Logger;

public class Log {

    private static Logger logger;

    private static boolean debug = false;
    private static boolean debugSQL =false;
    private static boolean debugTasks =false;

    public static void info(String text){
        logger.info(text);
    }

    public static void warn(String text){
        logger.warning(text);
    }

    public static void err(String text){
        logger.severe(text);
    }

    public static void debug(String text) {
        if(debug)
            logger.fine(text);
    }

    public static void debugSQL(String text) {
        if(debugSQL)
            logger.fine(text);
    }

    public static void debugTasks(String text) {
        if(debugTasks)
            logger.fine(text);
    }

    public static void setLogger(Logger logger, boolean debug, boolean debugSQL, boolean debugTasksb){
        Log.logger = logger;
        Log.debug = debug;
        Log.debugSQL = debugSQL;
        Log.debugTasks = debugTasks;
    }
}
