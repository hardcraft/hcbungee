package com.gmail.val59000mc.hcbungee.util;

import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;

/**
 * @author DarkSeraphim
 **/
public class Texts {
	public static BaseComponent[] toJson(String message) {
		return TextComponent.fromLegacyText(message.replace("&", "§"));
	}
}