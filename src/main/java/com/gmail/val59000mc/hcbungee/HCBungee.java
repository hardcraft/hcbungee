package com.gmail.val59000mc.hcbungee;

import com.gmail.val59000mc.hcbungee.announcer.Announcer;
import com.gmail.val59000mc.hcbungee.antiswear.AntiSwearManager;
import com.gmail.val59000mc.hcbungee.api.HCBungeeAPI;
import com.gmail.val59000mc.hcbungee.api.impl.HCBungeeImpl;
import com.gmail.val59000mc.hcbungee.duel.DuelManager;
import com.gmail.val59000mc.hcbungee.sockets.Sockets;
import com.gmail.val59000mc.hcbungee.tablist.TabListListener;
import net.md_5.bungee.api.plugin.Plugin;

public class HCBungee extends Plugin{
	
	private AntiSwearManager asm;

    private HCBungeeAPI api;
	
    @Override
    public void onEnable() {

        api = new HCBungeeImpl(this).loadNetwork();
    	
    	// load tab list listener
    	getProxy().getPluginManager().registerListener(this, new TabListListener());

    	// load antiswear
    	asm = new AntiSwearManager(this);
    	asm.enable();
    	
    	// load announcer
    	Announcer announcer = new Announcer(this);
    	announcer.enable();

        // load sockets
        Sockets sockets = new Sockets(this);

        // load duel
        DuelManager duels = new DuelManager(this, sockets);

    	
    }
    
    @Override
    public void onDisable() {
    	asm.disable();
    }
}
