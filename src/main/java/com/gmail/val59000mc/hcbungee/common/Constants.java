package com.gmail.val59000mc.hcbungee.common;

public class Constants {
    public static final boolean DEFAULT_MYSQL_ENABLED = false;
    public static final boolean DEFAULT_DEBUG_ENABLED = false;
    public static final boolean DEFAULT_DEBUG_SQL_ENABLED = false;
    public static final boolean DEFAULT_DEBUG_TASKS_ENABLED = false;
}
