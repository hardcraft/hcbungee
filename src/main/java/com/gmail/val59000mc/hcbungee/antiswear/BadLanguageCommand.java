package com.gmail.val59000mc.hcbungee.antiswear;

import java.io.IOException;
import java.util.Arrays;

import com.gmail.val59000mc.hcbungee.util.Texts;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

public class BadLanguageCommand extends Command{

	private AntiSwearManager asm;
	
	public BadLanguageCommand(AntiSwearManager asm) {
		super("lang", "hcbungee.antiswear.admin" );
		this.asm = asm;
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if(args.length > 0){
			
			switch(args[0]){
				case "add":
					
					if(args.length < 2){
						message(sender,"&cUtilise &7/lang add <expression> &cpour ajouter une expression de &elangage incorrect&c.");
						return;
					}
					
					String addWords = String.join(" ", Arrays.copyOfRange(args, 1, args.length));
					
					boolean added = asm.addBadLanguage(addWords);
					
					if(added)
						message(sender,"&aL'expression &f"+addWords+" &aa été ajoutée à la liste de &elangage incorrect&a.");
					else
						message(sender,"&cL'expression &f"+addWords+" &cest déjà dans la liste de &elangage incorrect&c.");
					
					break;
				case "remove":
					if(args.length < 2){
						message(sender,"&cUtilise &7/lang remove <expression> &cpour retirer une expression de &elangage incorrect&c.");
						return;
					}
					
					String removeWords = String.join(" ", Arrays.copyOfRange(args, 1, args.length));

					boolean removed = asm.removeBadLanguage(removeWords);
					
					if(removed)
						message(sender,"&aL'expression &f"+removeWords+" &aa été retirée de la liste de &elangage incorrect&a.");
					else
						message(sender,"&cL'expression &f"+removeWords+" &cn'est pas dans la liste de &elangage incorrect&c.");
					
					break;
				case "list":
					
					String badLanguages = "";
					
					if(args.length > 1){
						String containing = args[1];
						badLanguages =  asm.concatBadLanguagesContaining(containing);
						message(sender,"&e&lListe de langage incorrect contenant &7"+containing+" &e&l:");
					}else{
						badLanguages =  asm.concatBadLanguages();
						message(sender,"&e&lListe de langage incorrect :");
					}
					
					message(sender, (badLanguages.length() == 0 ? "&7Aucun" : badLanguages ) );
					
					break;
				case "reload":
					
					try {
						asm.reloadBadLanguagesFromConfig();
						message(sender,"&aListe de &elangage incorrect &arechargées depuis la config!");
					} catch (IOException e) {
						message(sender,"&cImpossible de recharger la liste de &elangage incorrect &cdepuis la config");
						e.printStackTrace();
					}
					
					break;
				default:
					displayHelp(sender);
					break;
			}
		}else{
			displayHelp(sender);
		}
	}

	private void message(CommandSender sender, String string) {
		sender.sendMessage(Texts.toJson(string));
	}
	
	private void displayHelp(CommandSender sender){
		message(sender,"&cCommande inconnue, liste des commandes  possibles : ");
		message(sender,"&c- &7/lang add <expression> &c: ajouter une expression de langage incorrect");
		message(sender,"&c- &7/lang remove <expression> &c: retirer une expression de langage incorrect");
		message(sender,"&c- &7/lang list [contenant] &c: lister des expressions de langage incorrect contenant des mots");

	}

}
