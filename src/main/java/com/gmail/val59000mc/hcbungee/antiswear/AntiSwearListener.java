package com.gmail.val59000mc.hcbungee.antiswear;

import java.util.concurrent.TimeUnit;

import com.gmail.val59000mc.hcbungee.util.Texts;

import fr.Alphart.BAT.BAT;
import fr.Alphart.BAT.Modules.InvalidModuleException;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class AntiSwearListener implements Listener{
	
	private AntiSwearManager asm;
	private BAT bat;
	
	private static BaseComponent[] badLanguageBlockedMessage;
	private static String swearPrefix = "&6[Ins] ";
	private static String badLanguagePrefix = "&e[Lang] ";
	public static String swearReason = "insultes [automatique]";
	private static String GLOBAL_SERVER = "(global)";
	
	public AntiSwearListener(AntiSwearManager asm){
		this.asm = asm;
		
		this.bat = (BAT) ProxyServer.getInstance().getPluginManager().getPlugin("BungeeAdminTools");
		
		badLanguageBlockedMessage = Texts.toJson(
			"&e&l[ATTENTION] &r&cSois &fpoli &cet &fcourtois &cou le &fban &csera pour toi !"
		);
		
	}
	
	@EventHandler
	public void onPlayerChat(ChatEvent event){
		
		if(event.isCancelled()){
			return;
		}
		
		if(event.getSender() instanceof ProxiedPlayer){
			ProxiedPlayer player = (ProxiedPlayer) event.getSender();
			String message = event.getMessage();
			if(!message.startsWith("/ins") && !message.startsWith("/lang")){
				AntiSwearResponse response = asm.checkMessage(event.getMessage());
				
				if(response.isCensored()){

					event.setCancelled(true);
					if(event.isCommand())
						processCensoredCommand(player, response);
					else
						processCensoredMessage(player, response);
						
				}
				
			}
		}
	}

	/**
	 * Commands that contains bad-languages or swears are just blocked
	 * @param player
	 * @param response
	 */
	private void processCensoredCommand(ProxiedPlayer player, AntiSwearResponse response) {
		player.sendMessage(badLanguageBlockedMessage);
		log(player, response);
		
	}
	
	/**
	 * Messages that contains bad-languages only are just blocked and a message is displayed to the player
	 * Messages that contains swears or swears + bad-languages are blocked and logged to file and trigger a staff warning if the warn limit is exceeded
	 * @param player
	 * @param response
	 */
	private void processCensoredMessage(ProxiedPlayer player, AntiSwearResponse response) {
		
		if(response.containsSwears()){

			if(!player.hasPermission("hcbungee.antiswear.nopunishment")){
				warnSwear(player);
				muteSwear(player);
				
				log(player, response);
			}
			swearMessage(player);
			
		}else if(response.containsBadLanguages()){
			player.sendMessage(badLanguageBlockedMessage);
			log(player, response);
		}
	}
	
	private void swearMessage(ProxiedPlayer player) {

		
		ProxyServer.getInstance().getScheduler().schedule(asm.getPlugin(), () ->{
			
			long warnCount = asm.countSwears(player.getName());
			
			if(player.isConnected()){
				
				asm.displayWarnsLimits(player);
				
				player.sendMessage(Texts.toJson("&cTu as actuellement &f"+warnCount+" &cwarns pour insultes"));
			}
			
			
			//Bat bat = BAT.getInstance().getModules().geti
		}, 1000, TimeUnit.MILLISECONDS);
		
		
		
	}

	private void log(ProxiedPlayer player, AntiSwearResponse response) {
		
		StringBuilder builder = new StringBuilder();
		if(response.containsSwears()){
			builder.append(swearPrefix);
		}
		if(response.containsBadLanguages()){
			builder.append(badLanguagePrefix);
		}
		
		builder.append("&c");
		builder.append(player.getServer().getInfo().getName());
		builder.append(" - ");
		builder.append(player.getName());
		builder.append(" : &r");
		builder.append(response.getCensoredMessage());
		String warnMessage = builder.toString();
		
		asm.logToFile(warnMessage);
		
	}

	private void warnSwear(ProxiedPlayer player){
		ProxyServer.getInstance().getPluginManager().dispatchCommand(ProxyServer.getInstance().getConsole(), "warn "+player.getName()+" "+swearReason);
	}

	private void muteSwear(ProxiedPlayer player){
		// delay mute to allow automatic trigger
		
		ProxyServer.getInstance().getScheduler().schedule(asm.getPlugin(), () ->{
			if(bat != null){

				try {
					if(player.isConnected() 
						&& bat.getModules().getMuteModule().isMute(player, GLOBAL_SERVER) == 0
						){
						ProxyServer.getInstance().getPluginManager().dispatchCommand(ProxyServer.getInstance().getConsole(), "gtempmute "+player.getName()+" "+15+"m "+swearReason);
					}
					
				} catch (InvalidModuleException e) {
					e.printStackTrace();
				}
			}
			if(player.isConnected()){
			}
		}, 1000, TimeUnit.MILLISECONDS);
	}
}
