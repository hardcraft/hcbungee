package com.gmail.val59000mc.hcbungee.antiswear;

public class AntiSwearResponse {
	private String censoredMessage;
	private boolean containsSwears;
	private boolean containsBadLanguages;
	
	public AntiSwearResponse(String censoredMessage, boolean containsSwears, boolean containsBadLanguages) {
		super();
		this.censoredMessage = censoredMessage;
		this.containsSwears = containsSwears;
		this.containsBadLanguages = containsBadLanguages;
	}

	public String getCensoredMessage() {
		return censoredMessage;
	}

	public boolean isCensored() {
		return containsSwears || containsBadLanguages;
	}

	public boolean containsSwears() {
		return containsSwears;
	}

	public boolean containsBadLanguages() {
		return containsBadLanguages;
	}
	
	
	
	
	
}
