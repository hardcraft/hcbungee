package com.gmail.val59000mc.hcbungee.antiswear;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.text.Normalizer;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import com.gmail.val59000mc.hcbungee.HCBungee;
import com.gmail.val59000mc.hcbungee.util.Texts;
import com.google.common.collect.Lists;
import com.google.common.io.Files;

import fr.Alphart.BAT.BAT;
import fr.Alphart.BAT.Modules.InvalidModuleException;
import fr.Alphart.BAT.Utils.UUIDNotFoundException;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

public class AntiSwearManager {

	private HCBungee plugin;
	private BAT bat;
	private Logger logger;
	private Writer fileLogger;
	private List<String> swears;
	private List<String> badLanguages;
	private List<BaseComponent[]> swearBlockedMessage;	
	
	private static String fileName = "swears.yml";
	
	
	public HCBungee getPlugin() {
		return plugin;
	}

	public AntiSwearManager(HCBungee plugin) {
		this.plugin = plugin;
		this.logger = plugin.getLogger();
		this.swears = Collections.synchronizedList(new ArrayList<String>());
		this.badLanguages = Collections.synchronizedList(new ArrayList<String>());
		this.bat = (BAT) ProxyServer.getInstance().getPluginManager().getPlugin("BungeeAdminTools");
		
		this.swearBlockedMessage = Lists.newArrayList(
			Texts.toJson("&6&l[ATTENTION] &r&cTu risques des &fsanctions &csi tu &finsultes &cdans le chat !"),
			Texts.toJson("&6&l- &r&f5 &cwarns = &fmute 6 heures"),
			Texts.toJson("&6&l- &r&f10 &cwarns = &fmute 24 heures"),
			Texts.toJson("&6&l- &r&f15 &cwarns = &fmute 2 jours"),
			Texts.toJson("&6&l- &r&f20 &cwarns = &fmute 1 semaine"),
			Texts.toJson("&6&l- &r&f25 &cwarns = &fmute 1 mois"),
			Texts.toJson("&6&l- &r&f30 &cwarns = &fmute à vie")
		);
	}
	
	///////////////////////////////
	/// enabling / disabling
	///////////////////////////

	public void enable() {

    	try {
    		reloadSwearsFromConfig();
    		reloadBadLanguagesFromConfig();
        	createLogFile();

	    	plugin.getProxy().getPluginManager().registerListener(plugin, new AntiSwearListener(this));
	    	plugin.getProxy().getPluginManager().registerCommand(plugin, new AntiSwearCommand(this));
	    	plugin.getProxy().getPluginManager().registerCommand(plugin, new BadLanguageCommand(this));
	    	
	    	// start recent swears cleanup task (clean censored message dates older than 1 hour)
//	    	plugin.getProxy().getScheduler().schedule(plugin, ()->{
//	    		synchronized (recentCensoredPlayers) {
//	    			ZonedDateTime ago = ZonedDateTime.now(ZoneOffset.UTC).minusMinutes(rememberSwearsForMinutes);
//	    			Iterator<Entry<UUID, List<ZonedDateTime>>> it = recentCensoredPlayers.entrySet().iterator();
//	    			while(it.hasNext()){
//		    			Entry<UUID, List<ZonedDateTime>> entry = it.next();
//
//		    			// remove old censored message dates
//		    			List<ZonedDateTime> dates = entry.getValue();
//		    			dates.removeIf((date)->date.isBefore(ago));
//		    			if(dates.isEmpty()){
//		    				it.remove(); // remove players without recent censored message dates
//		    			}
//	    			}
//				}
//	    	}, 0, 10, TimeUnit.MINUTES);
	    	
		} catch (IOException e) {
			// dont enable command and listener if unable to read/write files
			e.printStackTrace();
			logger.severe("Couldn't read or write config files");
		}
	}
	
	public void disable() {
    	try {
			this.closeLogFile();
		} catch (IOException e){
			e.printStackTrace();
			logger.severe("Couldn't close log file");
		}
	}
	
	// recent censored player check
	
//	/**
//	 * Store a new recent player warn in memory and check if its warn should be broadcast
//	 * @param uuid
//	 * @return true if more or equals than 5 swears warns in the last hour, false if less than 5 warns in the last hour
//	 */
//	public boolean rememberSwearAndCheckRecentWarns(UUID uuid){
//		synchronized (recentCensoredPlayers) {
//			ZonedDateTime now = ZonedDateTime.now(ZoneOffset.UTC);
//			if(recentCensoredPlayers.containsKey(uuid)){
//				List<ZonedDateTime> recentWarns = recentCensoredPlayers.get(uuid);
//				recentWarns.add(now);
//				if(recentWarns.size() >= 5){
//					return true;
//				}
//			}else{
//				recentCensoredPlayers.put(uuid, Lists.newArrayList(now));
//			}
//			return false;
//		}
//	}
	
	
	///////////////////////////////
	/// read / write config
	///////////////////////////
	
	// reload from config
	
	protected void reloadSwearsFromConfig() throws IOException {
		swears = reloadWordsFromConfig("swears");
		saveSwearsToConfig();
	}
	
	protected void reloadBadLanguagesFromConfig() throws IOException {
		badLanguages = reloadWordsFromConfig("bad-languages");
		saveBadLanguagesToConfig();
	}
	
	private List<String> reloadWordsFromConfig(String configNode) throws IOException{
        File file = new File(plugin.getDataFolder(), fileName);
     
        if (!file.exists()) {
            try (InputStream in = plugin.getResourceAsStream(fileName)) {
                java.nio.file.Files.copy(in, file.toPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        
		Configuration cfg = ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);
		List<String> wordsList = cfg.getStringList(configNode);
		
		List<String> normalizedWords = new ArrayList<String>();
		
		if(wordsList != null){
			for(String word : wordsList){
				normalizedWords.add(normalize(word));
			}
		}
		
		return normalizedWords;
	}

	
	// save to config file
	
	private void saveSwearsToConfig() {
		saveToConfig(swears, "swears");
	}

	private void saveBadLanguagesToConfig() {
		saveToConfig(badLanguages, "bad-languages");
	}
	
	private void saveToConfig(List<String> words, String configNode) {
		synchronized(words){
			try {
				File file = new File(plugin.getDataFolder(), fileName);
				ConfigurationProvider provider = ConfigurationProvider.getProvider(YamlConfiguration.class);
				Configuration cfg = provider.load(file);
				Collections.sort(words);
				cfg.set(configNode, words);
				provider.save(cfg, file);
			} catch (IOException e) {
				logger.severe("Couldn't load node "+configNode+" in config file "+fileName);
			}
		}
	}
	
	

	///////////////////////////////
	/// log file
	///////////////////////////
	
	private void createLogFile() throws IOException {
		File dir = new File(plugin.getDataFolder(),"swears-logs");
		dir.mkdirs();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH.mm.ss");
		String date = LocalDateTime.now().format(formatter);
		File log = new File(dir, date+".log");
		log.createNewFile();
		fileLogger = new BufferedWriter(new FileWriter(log,true));
		
		// remove old log files (>30 days)
		Long nowMinusDaysTimestamp = 1000*LocalDateTime.now().minusDays(30).toEpochSecond(ZoneOffset.UTC);
		File[] files = dir.listFiles((file) -> {
			boolean hasExtension = Files.getFileExtension(file.getName()).equalsIgnoreCase("log");
			boolean isOlderThanDays = file.lastModified() < nowMinusDaysTimestamp;
			return hasExtension && isOlderThanDays;
		});
		plugin.getLogger().info(files.length+" files marked for removal");
		for(File file : files){
			logger.info("Removing file '"+file.getName()+"'");
			file.delete();
		}
		
	}

	private void closeLogFile() throws IOException {
		if(fileLogger != null){
			fileLogger.close();
		}
		
	}

	protected void logToFile(String warnMessage) {
		if(fileLogger == null)
			return;
		
		ProxyServer.getInstance().getScheduler().runAsync(plugin, ()->{
			
			String date = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
			String logLine = date+" "+ChatColor.stripColor(ChatColor.translateAlternateColorCodes('&', warnMessage))+System.lineSeparator();
			try {
				fileLogger.append(logLine);
				fileLogger.flush();
			} catch (Exception e){
				logger.warning("BungeAntiSwear: Couldn't log message to log file. Disabling logger.");
				try {
					fileLogger.close();
				} catch (Exception e1) {
					e1.printStackTrace();
				} finally{
					fileLogger = null;
				}
			}
		});		
	}

	
	//////////////////////////////////////
	// swears and bad language checking
	/////////////////////////////////////
	
	protected AntiSwearResponse checkMessage(String message) {
		
		String normalized = normalize(message);
		
		boolean containsSwears = false;
		boolean containsBadLanguages = false;
		
		if(normalized.length() > 0){
			
			String afterAntiSwear = highlightWords(swears, "&6", normalized);
			// check contains swears
			if(!afterAntiSwear.equals(normalized)){
				containsSwears = true;
			}
			normalized = afterAntiSwear;
			
			String afterAntiBadLanguage = highlightWords(badLanguages, "&e", normalized);
			// check contains swears
			if(!afterAntiBadLanguage.equals(normalized)){
				containsBadLanguages = true;
			}
			normalized = afterAntiBadLanguage;
			
		}
		
		return new AntiSwearResponse(normalized, containsSwears, containsBadLanguages);
	}
	
	private String highlightWords(List<String> forbiddenWord, String color, String normalized){
		String msg = normalized;
		synchronized(forbiddenWord){
			for(String word : forbiddenWord){
				//now check if it surrounded by spaces
				if ( msg.equals(word) || msg.contains(" "+word+" ") || msg.startsWith(word+" ") || msg.endsWith(" "+word) ){
					msg = colorize(msg, color, word);
				}
			}
		}
		return msg;
	}


	// add
	
	protected boolean addSwear(String word) {
		boolean added = add(swears, word);
		if(added){
			saveSwearsToConfig();
		}
		return added;
	}
	
	protected boolean addBadLanguage(String word) {
		boolean added = add(badLanguages, word);
		if(added){
			saveBadLanguagesToConfig();
		}
		return added;
	}
	
	private boolean add(List<String> words, String word){
		String normalized = normalize(word);
		
		synchronized(words){
			if(words.contains(normalized))
				return false;
			
			words.add(normalized);
		}
		
		return true;
	}
	
	
	// remove
	
	protected boolean removeSwear(String word) {
		boolean removed = remove(swears, word);
		if(removed){
			saveSwearsToConfig();
		}
		return removed;
	}
	
	protected boolean removeBadLanguage(String word) {
		boolean removed = remove(badLanguages, word);
		if(removed){
			saveBadLanguagesToConfig();
		}
		return removed;
	}
	
	private boolean remove(List<String> words, String word){
		String normalized = normalize(word);
		
		synchronized(words){
			if(!words.contains(normalized))
				return false;
			
			words.remove(normalized);
		}
		
		return true;
	}
	
	// count
	
	public Long countSwears(String playerName) {

		Long warnCount = 0L;
		if(bat != null){
			try {
				warnCount = bat.getModules()
					.getCommentModule()
					.getComments(playerName)
					.stream()
					.filter(c -> c.getContent().contains(AntiSwearListener.swearReason))
					.count();
				
			} catch (InvalidModuleException e) {
				e.printStackTrace();
			}catch(UUIDNotFoundException e){
				warnCount = null;
			}
		}
		
		return warnCount;
		
	}
	
	public void displayWarnsLimits(CommandSender player){
		for(BaseComponent[] comp : swearBlockedMessage){
			player.sendMessage(comp);
		}
	}
	
	// concat
	
	protected String concatSwears() {
		return concat(swears);
	}
	
	protected String concatBadLanguages() {
		return concat(badLanguages);
	}
	
	private String concat(List<String> words){
		return String.join(", ", words);
	}

	
	// concat containing
	
	protected String concatSwearsContaining(String containing) {
		return concatContaining(swears, containing);
	}

	protected String concatBadLanguagesContaining(String containing) {
		return concatContaining(badLanguages, containing);
	}

	private String concatContaining(List<String> words, String containing) {
		List<String> wordsStartingWith = new ArrayList<String>();
		
		synchronized(words){
			for(String word : words){
				if(word.contains(containing)){
					wordsStartingWith.add(colorize(word,"&b",containing));	
				}	
			}
		}
		
		return "&f"+String.join(" , ", wordsStartingWith);
	}
	
	
	// colorize & normalize
	
	private String colorize(String word, String color, String containing) {
		return word.replace(containing, color+"&n"+containing+"&r");
	}

	private String normalize(String input){
		String output = Normalizer.normalize(ChatColor.stripColor(input).toLowerCase(), Normalizer.Form.NFD);
		output = output.replaceAll("[^\\w\\s]","");
		output = output.trim();
		return output;
	}
	
}
