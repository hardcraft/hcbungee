package com.gmail.val59000mc.hcbungee.antiswear;

import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.Callable;
import java.util.function.Consumer;

import com.gmail.val59000mc.hcbungee.util.Texts;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Command;

public class AntiSwearCommand extends Command{

	private AntiSwearManager asm;
	
	public AntiSwearCommand(AntiSwearManager asm) {
		super("ins", "hcbungee.antiswear.admin" );
		this.asm = asm;
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if(args.length > 0){
			
			switch(args[0]){
				case "add":
					
					if(args.length < 2){
						message(sender,"&cUtilise &7/lansng add <expression> &cpour ajouter une &6insulte&c.");
						return;
					}
					
					String addWords = String.join(" ", Arrays.copyOfRange(args, 1, args.length));
					
					boolean added = asm.addSwear(addWords);
					
					if(added)
						message(sender,"&aL'expression &f"+addWords+" &aa été ajoutée à la liste des &6insultes&a.");
					else
						message(sender,"&cL'expression &f"+addWords+" &cest déjà dans la liste des &6insultes&c.");
					
					break;
				case "remove":
					if(args.length < 2){
						message(sender,"&cUtilise &7/ins remove <expression> &cpour retirer une &6insulte&c.");
						return;
					}
					
					String removeWords = String.join(" ", Arrays.copyOfRange(args, 1, args.length));

					boolean removed = asm.removeSwear(removeWords);
					
					if(removed)
						message(sender,"&aL'expression &f"+removeWords+" &aa été retirée de la liste des &6insultes&a.");
					else
						message(sender,"&cL'expression &f"+removeWords+" &cn'est pas dans la liste des &6insultes&c.");
					
					break;
				case "list":
					
					String swears = "";
					
					if(args.length > 1){
						String containing = args[1];
						swears =  asm.concatSwearsContaining(containing);
						message(sender,"&6&lListe des insultes contenant &7"+containing+" &6&l:");
					}else{
						swears =  asm.concatSwears();
						message(sender,"&6&lListe des insultes :");
					}
					
					message(sender, (swears.length() == 0 ? "&7Aucun" : swears ) );
					
					break;
				case "lookup":
					if(args.length < 2){
						message(sender,"&cUtilise &7/ins lookup <joueur> &cpour lister les warns pour &6insultes& &cd'un joueur.");
						return;
					}else{
						String playerName = args[1];
						countSwears(sender, playerName);
					}
					break;
				case "limits":
					asm.displayWarnsLimits(sender);
					break;
				case "reload":
					
					try {
						asm.reloadSwearsFromConfig();
						message(sender,"&aListe des &6insultes &arechargées depuis la config!");
					} catch (IOException e) {
						message(sender,"&cImpossible de recharger la liste des &6insultes &cdepuis la config");
						e.printStackTrace();
					}
					
					break;
				default:
					displayHelp(sender);
					break;
			}
		}else{
			displayHelp(sender);
		}
	}

	private void countSwears(CommandSender sender, String playerName) {
		
		async(()->asm.countSwears(playerName), (totalSwears) ->{
			if(totalSwears == null){
				message(sender,"&cLe joueur &7"+playerName+" &cn'existe pas.");
			}else{
				message(sender,"&cLe joueur &7"+playerName+" &ca reçu &7"+totalSwears+" &cwarns pour &6insultes.&c");
			}
		});
		
	}
	
	public <T> void async(Callable<T> callable, Consumer<T> consumer) {
     
		ProxyServer.getInstance().getScheduler().runAsync(asm.getPlugin(), () ->{
			
			try {
				T result = callable.call();
				ProxyServer.getInstance().getScheduler().runAsync(asm.getPlugin(),()->consumer.accept(result));
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		});
    }

	private void message(CommandSender sender, String string) {
		sender.sendMessage(Texts.toJson(string));
	}
	
	private void displayHelp(CommandSender sender){
		message(sender,"&cCommande inconnue, liste des commandes  possibles : ");
		message(sender,"&c- &7/ins add <expression> &c: ajouter une insulte");
		message(sender,"&c- &7/ins remove <expression> &c: retirer une insulte");
		message(sender,"&c- &7/ins list [contenant] &c: lister les insultes contenant des mots");
		message(sender,"&c- &7/ins lookup <joueur> &c: afficher le total de warns pour insultes d'un joueur");
		message(sender,"&c- &7/ins limits &c: afficher les limites de warns.");

	}

}
