package com.gmail.val59000mc.hcbungee.event;

import com.gmail.val59000mc.hcbungee.api.HCBungeeAPI;
import net.md_5.bungee.api.plugin.Event;
import net.md_5.bungee.api.plugin.Plugin;

public abstract class HCBungeeEvent extends Event {

	private HCBungeeAPI api;

	public HCBungeeEvent(HCBungeeAPI api){
		this.api = api;
	}
	
	protected HCBungeeAPI getAPI(){
		return api;
	}
	
	protected Plugin getPlugin(){
		return api.getPlugin();
	}

}
