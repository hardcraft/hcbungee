package com.gmail.val59000mc.hcbungee.tablist;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class TabListListener implements Listener {

	private BaseComponent[] header;
	private BaseComponent[] footer;
	
	public TabListListener() {
		header = new ComponentBuilder("HardCraft").color(ChatColor.GREEN).bold(true).create();
		footer = new ComponentBuilder("Site Web: ").color(ChatColor.GREEN)
		        .append("www.hardcraft.fr").color(ChatColor.WHITE)
		        .append("\n TeamSpeak: ").color(ChatColor.GREEN)
		        .append("ts.hardcraft.fr").color(ChatColor.WHITE).create();
	}
	
	
	@EventHandler
	public void onPlayerJoin(PostLoginEvent e){
		e.getPlayer().setTabHeader(header,footer);
	}
}
